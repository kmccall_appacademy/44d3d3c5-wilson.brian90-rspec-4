class Temperature
  attr_accessor :in_fahrenheit
  attr_accessor :in_celsius
  # TODO: your code goes here!
  def initialize(hash)
    @in_fahrenheit = hash[:f]
    @in_celsius = hash[:c]

    if @in_fahrenheit == nil
      @in_fahrenheit = from_celsius(@in_celsius)
    end

    if @in_celsius == nil
      @in_celsius = from_fahrenheit(@in_fahrenheit)
    end

  end

  def self.from_celsius(cels)
    return Temperature.new({c: cels})
  end

  def self.from_fahrenheit(fah)
    return Temperature.new({f: fah})
  end

  def from_celsius(cels)
    (cels * (9 / 5.0)) + 32
  end

  def from_fahrenheit(fah)
    (fah - 32) * (5 / 9.0)
  end
end

class Fahrenheit < Temperature
  def initialize(fah)
    @in_fahrenheit = fah
    @in_celsius = from_fahrenheit(fah)
  end

end

class Celsius < Temperature
  def initialize(c)
    @in_celsius = c
    @in_fahrenheit = from_celsius(@in_celsius)
  end

end
