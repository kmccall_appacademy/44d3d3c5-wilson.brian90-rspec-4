class Book
  # TODO: your code goes here!
  def initialize()
  end

  def title=(string)
    array = string.split
    prepositions = ["and", "if", "the", "or", "in", "of", "a", "an"]
    array = array.map{ |word|
      if !prepositions.include?(word)
        word = word.capitalize
      else
        word = word
      end
    }
    array[0] = array[0].capitalize
    @title = array.join(" ")
  end

  def title
    @title
  end

end
