class Timer
  attr_accessor :seconds

  def initialize()
    @seconds = 0
  end

  def time_string
    @hours = @seconds / 3600
    @minutes = @seconds % 3600 / 60
    @remaining_seconds = (@seconds % 3600) % 60
    puts "remaining seconds: #{@remaining_seconds}"

    hrstring = ""
    if @hours > 9
      hrstring = @hours
    else
      hrstring = "0#{@hours}"
    end

    minstring = ""
    if @hours > 9
      minstring = @minutes
    else
      minstring = "0#{@minutes}"
    end

    secstring = ""
    if @remaining_seconds > 9
      secstring = @remaining_seconds
    else
      secstring = "0#{@remaining_seconds}"
    end

    return "#{hrstring}:#{minstring}:#{secstring}"
  end


end
