class Dictionary
  # TODO: your code goes here!
  attr_accessor :entries

  def initialize()
    @entries = {}
  end

  def add(hash)

    if hash.class == String
      @entries[hash] = nil
    end


    if hash.class == Hash
      array = hash.keys
      array.each do |key|
        @entries[key] = hash[key]
      end
    end

  end

  def keywords
    return @entries.keys.sort
  end

  def include?(term)
    @entries.keys.each do |i|
      if i == term
        return true
      end
    end
    return false
  end

  def find(term)
    returnHash = {}
    array = @entries.keys
    array.each do |key|
      if key.include?(term)
        returnHash[key] = @entries[key]
      end
    end
    returnHash
  end

  def printable
    array = []
    @entries = @entries.sort_by{|k, v| k}
    @entries.each do |k, v|
      array << "[#{k}] \"#{v}\""
    end
    array.join("\n")
  end

end
